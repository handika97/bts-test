import { createSlice } from "@reduxjs/toolkit";
import { post } from "./slice";

export const auth = createSlice({
  name: "auth",
  initialState: {
    isLogin: false,
    token:''
  },
  reducers: {
    startAsync: (state) => {
      state.loading = true;
    },
    stopAsync: (state) => {
      state.loading = false;
    },
    loginSuccess: (state, action) => {
        state.isLogin= true;
        state.token = action.payload;
    },
    Login: (state, action) => {
      console.log("yee")
        state.isLogin= true;
        state.token = "action.payload";
    },
    logout: (state) => {
      state.isLogin = false;
    },
  },
});

export const { startAsync, stopAsync, loginSuccess, logout, Login } = auth.actions;

export default auth.reducer;

// ---------------- ACTION ---------------

// import {errorMessage} from '../../utils';
const defaultBody = null;

export const login = (password, username) => (dispatch) => {
  dispatch(startAsync());
  dispatch(Login)
  console.log(password,username)
  dispatch(
    post(
      "http://18.141.178.15:8080/login",
      {
        username: username,
        password: password,
      },
      (res) => {
        console.log(res);
        dispatch(loginSuccess(res.data.data.token));
        alert(res.data.data.token, "berhasil")
        // history.replace("/app");
      },
      () => {
        // dispatch(stopAsync());
        alert('gagal login')
      },
      ()=>{
      }
    )
  );
};
