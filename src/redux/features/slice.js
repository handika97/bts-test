/* eslint-disable no-unused-vars */
import { createSlice } from "@reduxjs/toolkit";
import Axios from "axios";
import { loginSuccess } from "./authSlice";

export const slice = createSlice({
  name: "main",
  initialState: {},
  reducers: {},
});

export const {} = slice.actions;

export const post = (
  link,
  data,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {}
) => async (dispatch, getState) => {
  console.log("now", data, link);
  await Axios.post(link, data, {
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((res) => {
      console.log(res);

      ifSuccess(res);
    })
    .catch((err) => {
      ifError(err);
    })
    .finally(() => {
      finallyDo();
    });
};

export const postToken = (
  link,
  data,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {}
) => async (dispatch, getState) => {
  const { auth } = getState();

  console.log("now", data, link);
  await Axios.post(link, data, {
    headers: {
      "Content-Type": "application/json",
      authorization: "bearer" + auth.token,
    },
  })
    .then((res) => {
      console.log(res);

      ifSuccess(res);
    })
    .catch((err) => {
      ifError(err);
    })
    .finally(() => {
      finallyDo();
    });
};

export const get = (
  link,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {}
) => async (dispatch, getState) => {
  const { auth } = getState();
  console.log(auth.token);
  await Axios.post(link, {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + auth.token,
    },
  })
    .then((res) => {
      console.log(res);

      ifSuccess(res);
    })
    .catch((err) => {
      ifError(err);
    })
    .finally(() => {
      finallyDo();
    });
};

export default slice.reducer;
