import React, { Fragment, useState, useEffect } from "react";
import styled from "styled-components";
import Button from "../component/Button";
import { NavLink, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { login } from "../redux/features/authSlice";
import { post, get } from "../redux/features/slice";
import axios from "axios";
import { store} from "../redux/store";

const Login = () => {
  const dispatch = useDispatch();

  let history = useHistory();
  let auth = store.getState()
  console.log(auth);
console.log(store.getState());


  const [username, setusername] = useState("handika3");
  const [user, setuser] = useState("handika.yulma@gmail.com");
  const [password, setPass] = useState("123457");
  const [registrasi, setregistrasi] = useState(false);
  const [errMsg, setErrMsg] = useState("Oops, ada kesalahan.");

  useEffect(()=>{
      dispatch(get(
        "http://18.141.178.15:8080/checklist",
        (res) => {
          console.log(res);
          alert("Berhasil Get");
        },
        (err) => {
          alert("error Get");
          console.log(err.response);
        }
      ))
  },[])
  return (
    <Fragment>
      <Wrapper>
        <div className="login">
          <div className="hero">
            <h1>Home</h1>
          </div>
          <div className="login-form">
           </div>
        </div>
      </Wrapper>
    </Fragment>
  );
};

export default Login;

const Wrapper = styled.div`
  .loader {
    display: flex;
    justify-content: center;
    align-items: center;
    position: fixed;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    z-index: 1;
    background-color: rgba(0, 0, 0, 0.7);
    span {
      color: white;
      font-size: 2rem;
    }
  }
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  background-color: #e9ecef;
  .login {
    width: 1000px;
    height: 500px;
    display: flex;
    border-radius: 14px;
    overflow: hidden;
    box-shadow: 0 7px 8px rgba(0, 0, 0, 0.1);
  }
  .hero,
  .login-form {
    padding: 70px;
    height: 100%;
    h1 {
      font-size: 28px;
      font-weight: bold;
      margin-bottom: 5rem;
    }
  }
  .hero {
    width: 50%;
    background-color: white;
    text-align: center;
    display: flex;
    flex-direction: column;
    align-items: center;
    .brand {
      width: 300px;
    }
  }
  .login-form {
    width: 50%;
    min-height: 100px;
    background: linear-gradient(
      0deg,
      rgba(122, 65, 163, 0.9),
      rgba(122, 65, 163, 0.9)
    );
    color: white;
    transition: box-shadow 0.1s;
    h1 {
      color: white;
      margin-bottom: 3.7rem;
    }
    span {
      font-size: 14px;
    }
    form {
      margin-top: 1rem;
      .group-input {
        position: relative;
        margin-top: 1rem;
        background-color: white;
        width: 100%;
        border-radius: 4px;
        overflow: hidden;
        box-shadow: 0 6px 8px rgba(0, 0, 0, 0.07);
        &.focus {
          outline: none;
          border: none;
          box-shadow: 0 0 2px 3px rgba(99, 43, 141, 0.5);
        }
        .eye {
          cursor: pointer;
          position: absolute;
          right: 12px;
          top: 50%;
          transform: translateY(-50%);
          width: 20px;
          img {
            width: 100%;
          }
        }
        svg {
          width: 20px;
          fill: #632d8b;
        }
        label {
          position: absolute;
          left: 0;
          height: 100%;
          width: 100%;
        }
        .icon {
          position: absolute;
          left: 0;
          width: 46px;
          height: 100%;
          padding: 0 12px;
          border-right: 1.3px solid rgba(99, 43, 141, 0.2);
          display: flex;
          align-items: center;
          img {
            width: 100%;
          }
        }
        input {
          width: 100%;
          height: 100%;
          padding: 14px 14px 14px 64px;
          outline: none;
          border: none;
          font-size: 14px;
          font-weight: 600;
          &#password {
            padding-right: 50px;
          }
          &::placeholder {
            color: rgba(99, 43, 141, 0.5);
            font-weight: normal;
          }
        }
      }
      .error {
        margin-top: 1rem;
        border: 2px solid rgba(240, 83, 72);
        background-color: rgba(235, 64, 52, 0.07);
        padding: 12px 16px;
        border-radius: 4px;
        span {
          color: rgba(255, 234, 232);
        }
      }
      button {
        margin-top: 1rem;
      }
    }
  }
`;
