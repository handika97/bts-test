import React, { Fragment, useState, useEffect } from "react";
import styled from "styled-components";
import Button from "../component/Button";
import { NavLink, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { login } from "../redux/features/authSlice";
import { post } from "../redux/features/slice";
import axios from "axios";
import { store} from "../redux/store";

const Login = () => {
  const dispatch = useDispatch();

  let history = useHistory();
  let auth = store.getState()
  console.log(auth);
console.log(store.getState());


  const [username, setusername] = useState("handika3");
  const [user, setuser] = useState("handika.yulma@gmail.com");
  const [password, setPass] = useState("123457");
  const [registrasi, setregistrasi] = useState(false);
  const [errMsg, setErrMsg] = useState("Oops, ada kesalahan.");

  const onUserLogin = async () => {
    if (username !== "" && password !== "") {
      dispatch(login(password, username));
    }
  };
  const onUserRegister = () => {
    if (user !== "" && password !== "") {
      dispatch(
        post(
          "http://18.141.178.15:8080/register",
          { username: username, email: user, password: password },
          (res) => {
            console.log(res);
            alert("Berhasil Registrasi");
          },
          (err) => {
            alert("error registrasi", err.response);
            console.log(err.message);
          }
        )
      );
    }
  };
  return (
    <Fragment>
      <Wrapper>
        <div className="login">
          <div className="hero">
            <h1>Selamat Datang</h1>
          </div>
          <div className="login-form">
            <h1>Masuk</h1>
            <span>Silakan login terlebih dahulu</span>
            <form>
              <div className="group-input">
                <div className="icon icon-npp">
                  <label
                    htmlFor="                  Username
"
                  ></label>
                </div>
                <input
                  value={username}
                  onChange={(e) => setusername(e.target.value)}
                  type="text"
                  className="Username"
                  placeholder="Username"
                />
              </div>
              {registrasi && (
                <div className="group-input">
                  <div className="icon icon-npp">
                    <label htmlFor="npp"></label>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 109.4 109.42"
                    >
                      <g id="Layer_2" data-name="Layer 2">
                        <g id="Layer_1-2" data-name="Layer 1">
                          <g id="people">
                            <path
                              className="cls-1"
                              d="M54.7,55.76A27.88,27.88,0,1,0,26.82,27.88,27.92,27.92,0,0,0,54.7,55.76Zm0-43.13A15.25,15.25,0,1,1,39.45,27.88,15.26,15.26,0,0,1,54.7,12.63Z"
                            />
                            <path
                              className="cls-1"
                              d="M105.69,77.25q-3.54-2.22-7.27-4.12a96.35,96.35,0,0,0-87.44,0Q7.25,75,3.71,77.25A8,8,0,0,0,0,84v25.38a0,0,0,0,0,0,0H109.37a0,0,0,0,0,0,0V84A8,8,0,0,0,105.69,77.25ZM96.77,96.76a0,0,0,0,1,0,0H12.66a0,0,0,0,1,0,0V88.3a2.87,2.87,0,0,1,1.51-2.53,83.59,83.59,0,0,1,81.12,0,2.87,2.87,0,0,1,1.51,2.53Z"
                            />
                          </g>
                        </g>
                      </g>
                    </svg>
                  </div>
                  <input
                    value={user}
                    onChange={(e) => setuser(e.target.value)}
                    type="text"
                    id="Email"
                    className="Email"
                    placeholder="Email"
                  />
                </div>
              )}
              <div className="group-input">
                <div className="icon icon-npp">
                  <label htmlFor="password"></label>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 96.02 117.97"
                  >
                    <g id="Layer_2" data-name="Layer 2">
                      <g id="Layer_1-2" data-name="Layer 1">
                        <circle
                          className="cls-1"
                          cx="48.01"
                          cy="78.89"
                          r="10.96"
                        />
                        <path
                          className="cls-1"
                          d="M86,39.82H76.62V28.61c0,.25,0,.5,0,.74V27.72a28.6,28.6,0,0,0-57.17,0c0,.12,0,.24,0,.36V29c0-.12,0-.25,0-.37V39.82H10.08A10.08,10.08,0,0,0,0,49.9v58A10.07,10.07,0,0,0,10.08,118H86A10.07,10.07,0,0,0,96,107.9v-58A10.07,10.07,0,0,0,86,39.82ZM30.41,27.72h0a17.61,17.61,0,0,1,35.17,0h0v12.1H30.41ZM85,107H11V50.82H85Z"
                        />
                      </g>
                    </g>
                  </svg>
                </div>
                <input
                  value={password}
                  type="password"
                  onChange={(e) => setPass(e.target.value)}
                  id="password"
                  className="password"
                  placeholder="Password"
                />
              </div>
              {!registrasi ? (
                <div onClick={() => onUserLogin()}>
                  <p>masuk</p>
                </div>
              ) : (
                <div onClick={() => onUserRegister()}>
                  <p>Submit Registrasi</p>
                </div>
              )}
            </form>
            <Button
              onClick={() => {setregistrasi(!registrasi); console.log(auth)}}
              primary
              style={{ marginTop: 20 }}
            >
              Registrasi
            </Button>
          </div>
        </div>
      </Wrapper>
    </Fragment>
  );
};

export default Login;

const Wrapper = styled.div`
  .loader {
    display: flex;
    justify-content: center;
    align-items: center;
    position: fixed;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    z-index: 1;
    background-color: rgba(0, 0, 0, 0.7);
    span {
      color: white;
      font-size: 2rem;
    }
  }
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  background-color: #e9ecef;
  .login {
    width: 1000px;
    height: 500px;
    display: flex;
    border-radius: 14px;
    overflow: hidden;
    box-shadow: 0 7px 8px rgba(0, 0, 0, 0.1);
  }
  .hero,
  .login-form {
    padding: 70px;
    height: 100%;
    h1 {
      font-size: 28px;
      font-weight: bold;
      margin-bottom: 5rem;
    }
  }
  .hero {
    width: 50%;
    background-color: white;
    text-align: center;
    display: flex;
    flex-direction: column;
    align-items: center;
    .brand {
      width: 300px;
    }
  }
  .login-form {
    width: 50%;
    min-height: 100px;
    background: linear-gradient(
      0deg,
      rgba(122, 65, 163, 0.9),
      rgba(122, 65, 163, 0.9)
    );
    color: white;
    transition: box-shadow 0.1s;
    h1 {
      color: white;
      margin-bottom: 3.7rem;
    }
    span {
      font-size: 14px;
    }
    form {
      margin-top: 1rem;
      .group-input {
        position: relative;
        margin-top: 1rem;
        background-color: white;
        width: 100%;
        border-radius: 4px;
        overflow: hidden;
        box-shadow: 0 6px 8px rgba(0, 0, 0, 0.07);
        &.focus {
          outline: none;
          border: none;
          box-shadow: 0 0 2px 3px rgba(99, 43, 141, 0.5);
        }
        .eye {
          cursor: pointer;
          position: absolute;
          right: 12px;
          top: 50%;
          transform: translateY(-50%);
          width: 20px;
          img {
            width: 100%;
          }
        }
        svg {
          width: 20px;
          fill: #632d8b;
        }
        label {
          position: absolute;
          left: 0;
          height: 100%;
          width: 100%;
        }
        .icon {
          position: absolute;
          left: 0;
          width: 46px;
          height: 100%;
          padding: 0 12px;
          border-right: 1.3px solid rgba(99, 43, 141, 0.2);
          display: flex;
          align-items: center;
          img {
            width: 100%;
          }
        }
        input {
          width: 100%;
          height: 100%;
          padding: 14px 14px 14px 64px;
          outline: none;
          border: none;
          font-size: 14px;
          font-weight: 600;
          &#password {
            padding-right: 50px;
          }
          &::placeholder {
            color: rgba(99, 43, 141, 0.5);
            font-weight: normal;
          }
        }
      }
      .error {
        margin-top: 1rem;
        border: 2px solid rgba(240, 83, 72);
        background-color: rgba(235, 64, 52, 0.07);
        padding: 12px 16px;
        border-radius: 4px;
        span {
          color: rgba(255, 234, 232);
        }
      }
      button {
        margin-top: 1rem;
      }
    }
  }
`;
