import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";

import Login from "../src/pages/login"
import Home from "../src/pages/home"

const AuthRoute = ({ component: Component, isLogin, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isLogin ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/user/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

const App = () => {
  const isLogin = useSelector(state => state.auth.isLogin);

  // useEffect(() => {
  //   console.log(isLogin);
  // }, [isLogin]);

  return (
    <div className="h-100">
      <Router>
        <Switch>
          <AuthRoute path="/app" isLogin={isLogin} component={Login} />
          <Route path="/" component={Home} />
          {/* <Route path="/error" exact component={error} />
          <Route path="/" exact component={main} /> */}
          {/* <Redirect to="/error" /> */}
        </Switch>
      </Router>
    </div>
  );
};

export default App;
